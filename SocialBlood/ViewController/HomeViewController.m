//
//  HomeViewController.m
//  SocialBlood
//
//  Created by Nex Technology on 5/4/16.
//  Copyright © 2016 Nex. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController
@synthesize lbl_bloodType;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    lbl_bloodType.text=@"AB";
    lbl_bloodType.textColor=[UIColor whiteColor];
    lbl_bloodType.textAlignment=NSTextAlignmentCenter;
    lbl_bloodType.backgroundColor=[UIColor colorWithHexString:@"D9101B"];
    lbl_bloodType.layer.cornerRadius=50.0;
    lbl_bloodType.clipsToBounds=YES;
      [self cardSetup];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)cardSetup
{
    //[self.cardView setAlpha:1];
    /*self.cardView.layer.masksToBounds = NO;
     self.cardView.layer.cornerRadius = 1; // if you like rounded corners
     self.cardView.layer.shadowOffset = CGSizeMake(-.1f, .1f); //%%% this shadow will hang slightly down and to the right
     self.cardView.layer.shadowRadius = 0.5; //%%% I prefer thinner, subtler shadows, but you can play with this
     self.cardView.layer.shadowOpacity = 0.1; //%%% same thing with this, subtle is better for me*/
    
    self.cardView.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    self.cardView.layer.shadowOffset = CGSizeMake(1.f,1.f);
    self.cardView.layer.shadowRadius = 3.f;
    self.cardView.layer.shadowOpacity = .5f;
    self.cardView.layer.masksToBounds = NO;
  
    
    
    //%%% This is a little hard to explain, but basically, it lowers the performance required to build shadows.  If you don't use this, it will lag
    //UIBezierPath *path = [UIBezierPath bezierPathWithRect:self.cardView.bounds];
    //self.cardView.layer.shadowPath = path.CGPath;
    
    //self.backgroundColor = [UIColor colorWithRed:.9 green:.9 blue:.9 alpha:1]; //%%% I prefer choosing colors programmatically than on the storyboard
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
