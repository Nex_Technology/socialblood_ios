//
//  StringTable.m
//  SocialBlood
//
//  Created by Thazin Nwe on 4/5/16.
//  Copyright © 2016 Nex. All rights reserved.
//

#import "StringTable.h"

@implementation StringTable

NSString * const APP_BASED_LINK = @"https://social-blood.herokuapp.com/";
NSString * const USER_SIGNUP_LINK = @"api/users/sign_up";
NSString * const USER_LOGIN_FACEBOOK_LINK = @"api/users/fb_login";
NSString * const USER_LOGIN_LINK = @"api/users/login";
NSString * const USER_LOGOUT_LINK = @"api/users/logout";

NSString * const KEY_FOR_STATUS = @"status";
NSString * const KEY_FOR_RESPONSE = @"response";
NSString * const KEY_FOR_AUTH = @"user_auth";

int BLOODTYPE_O=1;
int BLOODTYPE_A=2;
int BLOODTYPE_B=3;
int BLOODTYPE_AB=4;


int BLOODRHTYPE_PLUS=0;
int BLOODRHTYPE_MINUS=1;
int BLOODRHTYPE_NONE=2;

int BLOODTYPE=1;
int BLOODRHTYPE=2;

@end
