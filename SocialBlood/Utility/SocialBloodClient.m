//
//  SocialBloodClient.m
//  SocialBlood
//
//  Created by Thazin Nwe on 4/5/16.
//  Copyright © 2016 Nex. All rights reserved.
//

#import "SocialBloodClient.h"
#import "User.h"

@implementation SocialBloodClient

+ (instancetype)sharedClient {
    
    static SocialBloodClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedClient = [[SocialBloodClient alloc] initWithBaseURL:[NSURL URLWithString:APP_BASED_LINK]];
        //[_sharedClient setSecurityPolicy:nil];
        NSMutableIndexSet* codes = [NSMutableIndexSet indexSetWithIndexesInRange: NSMakeRange(200, 100)];
        [codes addIndex: 401];
        [codes addIndex: 400];
        AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        _sharedClient.requestSerializer = serializer;
        _sharedClient.responseSerializer.acceptableStatusCodes = codes;
    });
    
    return _sharedClient;
}

+ (void) signUpUser:(User*)user withCompletionBlock:(void (^)(NSDictionary * results))block andFailureBlock:(void (^)(NSError * error))errorBlock{
    NSParameterAssert(block);
    NSParameterAssert(errorBlock);
    
    NSString * strLink = [NSString stringWithFormat:@"%@",USER_SIGNUP_LINK];
    
    NSDictionary * dicParameter;
    dicParameter = @{@"email":user.email,
                     @"password":user.password,
                     @"password_confirmation":user.comfirmPassword,
                     @"name":user.name,
                     @"phone_no":user.phoneNo,
                     @"blood_type_id":[NSNumber numberWithInteger:user.bloodTypeId],
                     @"blood_rh_type_id":[NSNumber numberWithInteger:user.bloodRhTypeId],
                     @"address":user.address,
                     @"latitude":[NSNumber numberWithDouble:user.latitude],
                     @"longitude":[NSNumber numberWithDouble:user.longitude],
                     @"about":user.about,
                     @"has_pet":[NSNumber numberWithInteger:user.hasPet],
                     @"pet":user.pet,
                     };
    NSLog(@"Sign Up User with dic %@",dicParameter);
   
    [[SocialBloodClient sharedClient] POST:strLink parameters:dicParameter progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"Sign Up return json : %@",responseObject);
        NSNumber * status ;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
        if ([httpResponse isKindOfClass:[NSHTTPURLResponse class]]) {
            status = [[NSNumber alloc]initWithInteger:httpResponse.statusCode];
        }
        
        block([self makeDictionaryWith:responseObject status:status]);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock(error);
        NSLog(@"Sign Up return error : %@",error.debugDescription);
    }];
}

+ (void) signInUserWithEmail:(NSString *)email password:(NSString *)password CompletionBlock:(void (^)(NSDictionary * results))block andFailureBlock:(void (^)(NSError * error))errorBlock{
    NSParameterAssert(block);
    NSParameterAssert(errorBlock);
    NSLog(@"Sign In User");
    NSString * strRestLink = [NSString stringWithFormat:@"%@",USER_LOGIN_LINK];
    NSDictionary * dicParameter;
    dicParameter = @{@"email":email,
                     @"password":password,
                     @"device_type":@"i"
                     };
    
    [[SocialBloodClient sharedClient] POST:strRestLink parameters:dicParameter progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"Sign In return json : %@",responseObject);
        NSNumber * status ;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
        if ([httpResponse isKindOfClass:[NSHTTPURLResponse class]]) {
            status = [[NSNumber alloc]initWithInteger:httpResponse.statusCode];
        }
        block([self makeDictionaryWith:responseObject status:status]);

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock(error);
        NSLog(@"Sign In return error : %@",error.debugDescription);
    }];
    
}

+ (NSDictionary *)makeDictionaryWith:(NSDictionary *)response status:(NSNumber *)status{
    NSDictionary * dic;
    if(response ==nil)
        dic= @{KEY_FOR_STATUS:status};
    else dic= @{KEY_FOR_STATUS:status,KEY_FOR_RESPONSE:response};
    return dic;
};

@end
