//
//  StringTable.h
//  SocialBlood
//
//  Created by Thazin Nwe on 4/5/16.
//  Copyright © 2016 Nex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringTable : NSObject{

}
extern NSString * const APP_BASED_LINK ;
extern NSString * const USER_SIGNUP_LINK;
extern NSString * const USER_LOGIN_FACEBOOK_LINK ;
extern NSString * const USER_LOGIN_LINK ;
extern NSString * const USER_LOGOUT_LINK ;

extern NSString * const KEY_FOR_STATUS ;
extern NSString * const KEY_FOR_RESPONSE ;
extern NSString * const KEY_FOR_AUTH ;

extern int BLOODTYPE_O;
extern int BLOODTYPE_A;
extern int BLOODTYPE_B;
extern int BLOODTYPE_AB;


extern int BLOODRHTYPE_PLUS;
extern int BLOODRHTYPE_MINUS;
extern int BLOODRHTYPE_NONE;

extern int BLOODTYPE;
extern int BLOODRHTYPE;


@end
