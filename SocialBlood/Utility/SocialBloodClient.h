//
//  SocialBloodClient.h
//  SocialBlood
//
//  Created by Thazin Nwe on 4/5/16.
//  Copyright © 2016 Nex. All rights reserved.
//

#import <Foundation/Foundation.h>

@class User;

@interface SocialBloodClient : AFHTTPSessionManager

+ (instancetype)sharedClient;

+ (void) signUpUser:(User*)user withCompletionBlock:(void (^)(NSDictionary * results))block andFailureBlock:(void (^)(NSError * error))errorBlock;

+ (void) signInUserWithEmail:(NSString *)email password:(NSString *)password CompletionBlock:(void (^)(NSDictionary * results))block andFailureBlock:(void (^)(NSError * error))errorBlock;

+ (NSDictionary *)makeDictionaryWith:(NSDictionary *)response status:(NSNumber *)status;

@end
