//
//  LocationViewController.m
//  SocialBlood
//
//  Created by Nex Technology on 6/4/16.
//  Copyright © 2016 Nex. All rights reserved.
//

#import "LocationViewController.h"
#import <CoreLocation/CoreLocation.h>




@interface LocationViewController (){
   
    NSArray * arrNearByRest;
    IBOutlet UITableView * tblView;
    BOOL isfound;
    CLGeocoder *geocoder;
    CGFloat currentLat;
    CGFloat currentLng;
}
@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation LocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated{
    
    
    [self startToGetLocation];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)startToGetLocation{
    [SVProgressHUD show];
    geocoder = [[CLGeocoder alloc] init];
    // Create the core location manager object
    _locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    // This is the most important property to set for the manager. It ultimately determines how the manager will
    // attempt to acquire location and thus, the amount of power that will be consumed.
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    // Once configured, the location manager must be "started"
    //
    // for iOS 8, specific user level permission is required,
    // "when-in-use" authorization grants access to the user's location
    //
    // important: be sure to include NSLocationWhenInUseUsageDescription along with its
    // explanation string in your Info.plist or startUpdatingLocation will not work.
    //
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}



- (void) onclose{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
    
}





- (IBAction)onClickBack:(id)sender {
    [ self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
@end
