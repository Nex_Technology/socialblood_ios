//
//  ViewController.m
//  SocialBlood
//
//  Created by Thazin Nwe on 4/4/16.
//  Copyright © 2016 Nex. All rights reserved.
//

#import "ViewController.h"
#import "User.h"
#import "LoginViewController.h"
#import "User.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated{

    if ([Utility stringIsEmpty:[User getUserAuthToken] shouldCleanWhiteSpace:YES]) {
        [self showLoginView];
    }
}

- (void) showLoginView{
    
    LoginViewController * vc = [[UIStoryboard storyboardWithName:@"Login" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self presentViewController:vc animated:YES completion:^{
    }];
}


@end
