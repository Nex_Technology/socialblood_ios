//
//  main.m
//  SocialBlood
//
//  Created by Thazin Nwe on 4/4/16.
//  Copyright © 2016 Nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
