//
//  HomeViewController.h
//  SocialBlood
//
//  Created by Nex Technology on 5/4/16.
//  Copyright © 2016 Nex. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "UIColor+expanded.h"
#import "LPPopupListView.h"
#import "LocationViewController.h"

@interface HomeViewController : UIViewController<LPPopupListViewDelegate>

@property (nonatomic, strong) IBOutlet UILabel * lbl_bloodType;
@property (nonatomic, strong) IBOutlet UIView * cardView;
@property int bloodType;
@property int bloodRHType;
- (IBAction)onClickEnterLocation:(id)sender;
- (IBAction)onClickSelectBlood:(id)sender;

@end
